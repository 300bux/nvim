" General Settings
source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim
source $HOME/.config/nvim/keys/which-key.vim

" Theme
source $HOME/.config/nvim/colors/noctu.vim

" Plugin Configs
source $HOME/.config/nvim/plug-config/coc.vim


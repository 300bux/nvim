" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

  " Autocompletion
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  " Key binds
  Plug 'liuchengxu/vim-which-key'

  " Emmet for HTML
  Plug 'mattn/emmet-vim'

  " Minimal UI
  Plug 'junegunn/goyo.vim'

call plug#end()
